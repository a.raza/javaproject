package fr.epita.quiz.resources;

import java.util.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.epita.maths.test.TestDI;
import fr.epita.quiz.datamodel.MCQChoice;
import fr.epita.quiz.datamodel.Question;
import fr.epita.quiz.services.data.MCQChoiceDAO;
import fr.epita.quiz.services.data.QuestionDAO;
import fr.epita.quiz.services.data.QuizDataservice;
import fr.epita.quiz.services.web.api.transport.MCQChoiceMessage;
import fr.epita.quiz.services.web.api.transport.QuestionMessage;

@Path(QuestionResource.PATH)
public class QuestionResource {
	
	static final String PATH = "questions";

	@Inject
	QuizDataservice quizDataService;
	
	@Inject
	QuestionDAO questionDAO;
	
	@Inject
	MCQChoiceDAO mcqChoiceDAO;
	
	/**
	 * Create Questions
	 */
	@POST
	@Path("/")
	@Consumes(value = { MediaType.APPLICATION_JSON })
	public Response createQuestion(QuestionMessage message) throws URISyntaxException {
 		
		//get the message in a form of Question entity
		Question question = toQuestion(message);
		
		//create question using the data service
		quizDataService.createQuestionWithChoices(question, toMCQChoiceList(message.getMcqChoices()));
		
		//create it on database
		return Response.created(new URI(PATH + "/" + String.valueOf(question.getId()))).build();
	}
	
	/*
	 * Get question by question lable
	 * */
	@GET
	@Path("/")
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Response findAllQuestions( @QueryParam("query") String inputString) {
		
		List<QuestionMessage> messages = new ArrayList<QuestionMessage>();
		
		//create new question and apply the key word to search for the that certain question
		Question question = new Question();
		
		question.setQuestionLabel(inputString);
		
		//find that certain question from LinkedHashMap
		Map<Question, List<MCQChoice>> myMap = quizDataService.findCertainQuestion(question);
		
		//convert questions to Question message.. for transition
		for(Entry<Question,List<MCQChoice>> entry : myMap.entrySet()) {
		
			QuestionMessage questionMessage = new QuestionMessage();
				
			questionMessage.setId(entry.getKey().getId());
			questionMessage.setQuestionLabel(entry.getKey().getQuestionLabel());
			questionMessage.setMcqChoices(toMCQChoiceMessageList(entry.getValue()));
			
			messages.add(questionMessage);
		}
		
		return Response.ok(messages).build();
	}
	
	/*
	 *update question by id
	 */
	@PUT
	@Path("/{id}")
	@Consumes(value = { MediaType.APPLICATION_JSON })
	public Response updateOneQuestion(QuestionMessage message) {
		
		Question question = questionDAO.getById(Long.valueOf(message.getId()));
		
		if (question == null) {
		
			return Response.status(Status.NOT_FOUND).entity("{\"message\" : 'Not found'}").build();
		}
		
		applyToQuestion(message, question);
		
		questionDAO.update(question);
		
		return Response.ok(message).build();
	}
	
	/*
	 * Delete Function for deleting question...
	 * */
	@DELETE
	@Path("/delete")
	@Produces(value = { MediaType.APPLICATION_JSON })
	public Response deleteCertainQuestion( String questionToDelete) {
		
		List<QuestionMessage> messages = new ArrayList<QuestionMessage>();
		
		Question question = new Question();
		
		question.setQuestionLabel(questionToDelete);
		
		if (questionToDelete == null) {
			return Response.status(Status.NOT_FOUND).entity("{\"message\" : 'Not found'}").build();
		}

		Map<Question, List<MCQChoice>> map = quizDataService.deleteCertainQuestions(question);
		
		for(Entry<Question,List<MCQChoice>> entry : map.entrySet()) {
			
			QuestionMessage questionMessage = new QuestionMessage();
			
			questionMessage.setId(entry.getKey().getId());
			
			questionMessage.setQuestionLabel(entry.getKey().getQuestionLabel());
			
			questionMessage.setMcqChoices(toMCQChoiceMessageList(entry.getValue()));	
			
			messages.add(questionMessage);
		}
		
		return Response.ok(messages).build();
	}
	
	/*
	 * will aid in converting MCQ Choice list to the MCQ Choice Message List
	 * */
	private static List<MCQChoiceMessage> toMCQChoiceMessageList(List<MCQChoice> list) {
		List<MCQChoiceMessage> mcqList= new ArrayList<>();
		for (MCQChoice mcq : list) {
			mcqList.add(fromMCQChoiceToMessagee(mcq));
		}
		return mcqList;
	}
	
	/*
	 * will add MCQChoiceList To QuestionMessage
	 * */
	private void addMCQChoiceListToQuestionMessage(List<MCQChoice>list, QuestionMessage qm) {
		
		List<MCQChoiceMessage> resultList = new ArrayList<>();
		
		for (MCQChoice choice : list) {
			
			choice.setId(qm.getId());
		
			resultList.add(fromMCQChoice(choice));
		}

		qm.setMcqChoices(resultList);
	}

	/*
	 * will aid in converting the MCQ Choice Message to MCQ Choice 
	 * */
	private static MCQChoiceMessage fromMCQChoiceToMessagee(MCQChoice mcq) {
		MCQChoiceMessage mcqm=new MCQChoiceMessage();
		mcqm.setLabel(mcq.getChoiceLabel());
		mcqm.setValid(mcq.getValid());
		mcqm.setId(mcq.getId());
 		return mcqm;
 	}
	
	/*
	 * will aid in converting the QuestionMessage to Question
	 * */
	private static Question toQuestion(QuestionMessage qm) {
		
		Question question = new Question();
		
		question.setId(qm.getId());
		
		question.setQuestionLabel(qm.getQuestionLabel());
		
		return question;
	}
	
	/*
	 * will convert toMCQChoiceList to MCQChoiceMessage List
	 * */
	private static List<MCQChoice> toMCQChoiceList(List<MCQChoiceMessage> list) {
		
		List<MCQChoice> mcqList=new ArrayList<>();
		for (MCQChoiceMessage mcq : list) {
		
			mcqList.add(fromMCQChoiceMessageeToMCQ(mcq));
		}	
		return mcqList;
	}
	
	/*
	 * will aid in converting the Question to QuestionMessage
	 * */
	private static QuestionMessage fromQuestion(Question question) {
		
		QuestionMessage questionMessage = new QuestionMessage();
		
		questionMessage.setId(question.getId());
		
		questionMessage.setQuestionLabel(question.getQuestionLabel());
		
		return questionMessage;
	}
	
	/*
	 * will set the question label from the question message to Question 
	 * */
	private static void applyToQuestion(QuestionMessage qmessage, Question question) {
		
		question.setQuestionLabel(qmessage.getQuestionLabel());
	
	}
	
	/*
	 * will convert to MCQchoice from MCQchoice Message
	 * */
	private static MCQChoice toMCQChoice(MCQChoiceMessage mcqChoiceMessage) {
		
		MCQChoice mcqChoice = new MCQChoice();
	
		mcqChoice.setId(mcqChoiceMessage.getId());
		
		mcqChoice.setChoiceLabel(mcqChoiceMessage.getLabel());
		
		return mcqChoice;
	}
	
	/*
	 * will convert from MCQChoiceMessagee To MCQ
	 * */
	private static MCQChoice fromMCQChoiceMessageeToMCQ(MCQChoiceMessage mcqm) {
	
		MCQChoice mcq=new MCQChoice();
		
		mcq.setId(mcqm.getId());
		
		mcq.setChoiceLabel(mcqm.getLabel());
		
		mcq.setValid(mcqm.getValid());
		
		mcq.setQuestion(mcq.getQuestion());
		
		return mcq;
	}
	
	/*
	 * will convert to MCQChoiceMessage from MCQChoice
	 * */
	private static MCQChoiceMessage fromMCQChoice(MCQChoice mcqChoice) {
		
		MCQChoiceMessage mcqChoiceMessage = new MCQChoiceMessage();
		
		mcqChoiceMessage.setId(mcqChoice.getId());
		
		mcqChoiceMessage.setLabel(mcqChoice.getChoiceLabel());
		
		return mcqChoiceMessage;
	}
	
}