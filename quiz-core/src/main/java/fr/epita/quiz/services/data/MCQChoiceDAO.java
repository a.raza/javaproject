package fr.epita.quiz.services.data;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import fr.epita.quiz.datamodel.MCQChoice;

/*
 * The MCQ DAO that is extended by the Generic DAO
 * */
@Repository
public class MCQChoiceDAO extends GenericDAO<MCQChoice> {

	private static final Logger LOGGER = LogManager.getLogger(MCQChoiceDAO.class);

	/*
	 * following the search rule applied in the Generic DAO
	 * will return MCQChoice Type
	 * */
	public List<MCQChoice> search(MCQChoice mcqChoiceCriteria) {
		
		Query<MCQChoice> searchQuery = getSession().createQuery("from MCQChoice", MCQChoice.class);
		return searchQuery.list();	
	
	}
	
	/*
	 * will return the type
	 * */
	@Override
	public Class<MCQChoice> getType() {
		return MCQChoice.class;
	}


	

}
