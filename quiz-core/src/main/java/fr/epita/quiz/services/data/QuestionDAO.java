package fr.epita.quiz.services.data;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import fr.epita.quiz.datamodel.MCQChoice;
import fr.epita.quiz.datamodel.Question;

/*
 * The Question DAO that is extended by the Generic DAO
 * */
@Repository
public class QuestionDAO extends GenericDAO<Question>{

	//to get the log statement
	private static final Logger LOGGER = LogManager.getLogger(QuestionDAO.class);

	/*
	 * following the search rule applied in the Generic DAO
	 * will return MCQChoice Type
	 * */
	public List<Question> search(Question question) {
		
		Query<Question> search = getSession().createQuery("from Question where questionLabel like :inputString ", Question.class);
		search.setParameter("inputString", "%"+question.getQuestionLabel()+"%");
		return search.list();
		
	}
	
	/*
	 * will return true if certain question label is available
	 * */
	public boolean isQuestionAvailable(String qLabel) {
		
		Query<Question> searchItem = getSession().createQuery("from Question where questionLabel like :inputString ", Question.class);
		
		searchItem.setParameter("inputString", "%"+qLabel+"%");
		
		int size = searchItem.list().size();
		
		if(size > 0) 
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}

	/*
	 * will Question type
	 * */
	@Override
	public Class<Question> getType() {
		return Question.class;
	}


}
