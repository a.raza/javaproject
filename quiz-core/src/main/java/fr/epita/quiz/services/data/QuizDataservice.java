package fr.epita.quiz.services.data;

import java.io.Console;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import fr.epita.maths.test.TestDI;
import fr.epita.quiz.datamodel.MCQChoice;
import fr.epita.quiz.datamodel.Question;

/*
 * A Data service that manages data behind the scene 
 * */
@Repository
public class QuizDataservice {

	@Inject
	QuestionDAO questionDAO;
	
	@Inject
	MCQChoiceDAO mcqDAO;
	
	@Inject
	SessionFactory sessionFactory;
	
	/*
	 * creates Question with Choices
	 * */
	public void createQuestionWithChoices(Question question, List<MCQChoice> choices) {

		//open session
		Session session = sessionFactory.openSession();
		
		//begin transaction
		Transaction tx = session.beginTransaction();

		//check if the question is already available in the data base
		if(!questionDAO.isQuestionAvailable(question.getQuestionLabel())) {
			
			//it does not exist... create new
			questionDAO.create(question);
		
			//create the mcq choices
			for (MCQChoice choice : choices) {
				
				//set question to the mcq choice
				choice.setQuestion(question);
				
				//set the mcq choice
				mcqDAO.create(choice);
			}
			
		}
		
		//comit
		tx.commit();
		
		//close
		session.close();
	}
	
	//will return map with certain question from the database
	public Map<Question,List<MCQChoice>> findCertainQuestion(Question question) {
		
		//get the list of question and their Mcqs
		Map<Question,List<MCQChoice>> questionsAndChoices = new LinkedHashMap<Question,List<MCQChoice>>(); 
		
		//search for certain question
		List<Question> list = questionDAO.search(question);

		//create new mcq choice list
		List<MCQChoice> mcqlist = new ArrayList<MCQChoice>();
		
		//set question and it's mcq choices
		for(int i=0;i<list.size();i++) {
			
			MCQChoice mcqchoice=new MCQChoice();
			mcqchoice.setQuestion(question);
			mcqlist=mcqDAO.search(mcqchoice);
			questionsAndChoices.put(list.get(i), mcqlist);

		}
		//return..
		return questionsAndChoices;
		
	}
	
	//will return the map after deleting certain question from the map
	public Map<Question, List<MCQChoice>> deleteCertainQuestions(Question question) {
		
		Map<Question,List<MCQChoice>> questionsAndChoices = new LinkedHashMap<Question,List<MCQChoice>>(); 
		
		List<Question> myQuestionlist = questionDAO.search(question);
		
		List<MCQChoice> mcqlist = new ArrayList<MCQChoice>();
		
		for(int i=0;i<myQuestionlist.size();i++) {
			
			MCQChoice mcqchoice=new MCQChoice();
			
			mcqchoice.setQuestion(question);
			
			mcqlist=mcqDAO.search(mcqchoice);
			
			questionsAndChoices.remove(myQuestionlist.get(i), mcqlist);

		}
		return questionsAndChoices;
	}
	
}
