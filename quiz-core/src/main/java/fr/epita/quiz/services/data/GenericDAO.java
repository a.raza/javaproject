package fr.epita.quiz.services.data;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/*
 * Generic DAO abstract that will be used to call REST API calls
 * */
public abstract class GenericDAO<T> {
	
	private static final Logger LOGGER = LogManager.getLogger(GenericDAO.class);

	/*
	 * which will contain all DB related property details pulled from either hibernate.cfg.xml file or hibernate.properties file
	 * */
	@Inject
	private SessionFactory sf;

	/*
	 * will update the type of instance it will get
	 * */
	public void update(T instance) {
		Session session = getSession();
		Commitable<Transaction> commitableTx = getTransaction(session);
		session.update(instance);
		commitableTx.commit();

	}
	/*
	 * will delete the type of instance it will get
	 * */
	public void delete(T instance) {
		Session session = getSession();
		Commitable<Transaction> commitableTx = getTransaction(session);
		session.delete(instance);
		commitableTx.commit();

	}
	/*
	 * will create as according to the type of instance it will get
	 * */
	public void create(T instance) {
		Session session = getSession();
		Commitable<Transaction> commitableTx = getTransaction(session);
		session.save(instance);
		commitableTx.commit();

	}

	/*
	 * will get commitable transaction from the session
	 * */
	protected final Commitable<Transaction> getTransaction(Session session) {
		final boolean areWeTheInitiatorOfTheTransaction = session.getTransaction() == null
				|| !session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE);
		final Transaction currentTransaction = areWeTheInitiatorOfTheTransaction ? session.beginTransaction()
				: session.getTransaction();

		return new Commitable<Transaction>() {

			/*
			 * will check if we are initiator of the transaction
			 * */
			public boolean isCommitable() {
				return areWeTheInitiatorOfTheTransaction;
			}

			/*
			 * will get available transaction
			 * */
			public Transaction getInstance() {
				return currentTransaction;
			}

			/*
			 * will apply the changes
			 * */
			public void commit() {
				if (areWeTheInitiatorOfTheTransaction) {
					currentTransaction.commit();
				}

			}
		};

	}

	/*
	 * will get the session
	 * */
	protected final Session getSession() {
		Session session = null;
		try {
			session = sf.getCurrentSession();
		} catch (HibernateException he) {
			LOGGER.warn("got an exception while trying to get the current session : {}", he.getMessage());
		}
		if (session == null) {
			session = sf.openSession();
		}
		return session;
	}
	
	/*
	 * will return list of certain type
	 * */
	public abstract List<T> search(T criteriaInstance);


	/*
	 * get the id 
	 * */
	public T getById(Serializable id) {
		return getSession().get(getType(), id);
	}
	
	public abstract Class<T> getType();
	
}
